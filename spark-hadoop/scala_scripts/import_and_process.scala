val hdfs_addr = "hdfs://" + sc.getConf.get("spark.driver.hdfs.ip") + ":9000"

val businesses = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load(hdfs_addr + "/data/Legally_Operating_Businesses.csv")
val inspections = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load(hdfs_addr + "/data/Inspections.csv")
val charges = spark.sqlContext.read.format("csv").option("header", "true").option("inferSchema", "true").load(hdfs_addr + "/data/Charges.csv")

businesses.printSchema
inspections.printSchema
charges.printSchema

inspections.groupBy("Inspection Result").count.write.format("csv").option("header", true).save(hdfs_addr + "/data/inspections_results.csv")

businesses.registerTempTable("businessesTable")
inspections.registerTempTable("inspectionsTable")
charges.registerTempTable("chargesTable")

val charged_businesses_with_expired_license = spark.sqlContext.sql("select `Business Name`, `Business Name 2`, `License Expiration Date`, `License Creation Date` from businessesTable where `Business Name` in (select `Business Name` from chargesTable) and `License Status` == 'Inactive'")

charged_businesses_with_expired_license.registerTempTable("ChBuWExLi")
charged_businesses_with_expired_license.write.format("csv").option("header", true).save(hdfs_addr + "/data/charged_businesses_with_expired_license.csv")

val inspections_count_for_charged_businesses_with_expired_license = spark.sqlContext.sql("select ChBuWExLi.`Business Name`, first(ChBuWExLi.`Business Name 2`), first(ChBuWExLi.`License Expiration Date`), first(ChBuWExLi.`License Creation Date`), count(*) from ChBuWExLi join inspectionsTable on ChBuWExLi.`Business Name` == inspectionsTable.`Business Name` group by ChBuWExLi.`Business Name`")
inspections_count_for_charged_businesses_with_expired_license.write.format("csv").option("header", true).save(hdfs_addr + "/data/inspections_count_for_charged_businesses_with_expired_license.csv")
