val hdfs_addr = "hdfs://" + sc.getConf.get("spark.driver.hdfs.ip") + ":9000"

val inspections_results = spark.sqlContext.read.format("csv").option("header", true).option("inferSchema", "true").load(hdfs_addr + "/data/inspections_results.csv")
val charged_businesses_with_expired_license = spark.sqlContext.read.format("csv").option("header", true).option("inferSchema", "true").load(hdfs_addr + "/data/charged_businesses_with_expired_license.csv")
val inspections_count_for_charged_businesses_with_expired_license = spark.sqlContext.read.format("csv").option("header", true).option("inferSchema", "true").load(hdfs_addr + "/data/inspections_count_for_charged_businesses_with_expired_license.csv")

inspections_results.show
charged_businesses_with_expired_license.show
inspections_count_for_charged_businesses_with_expired_license.show
