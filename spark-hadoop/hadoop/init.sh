#!/bin/bash  

echo "[$0] Launching /etc/bootstrap.sh"
/etc/bootstrap.sh
echo "[$0] Waiting 5 seconds"
sleep 5
export PATH=$PATH:/usr/local/hadoop/bin/

echo "[$0] Importing example data"
hdfs dfs -mkdir /data
hdfs dfs -put /etc/data/Legally_Operating_Businesses.csv /data/Legally_Operating_Businesses.csv
hdfs dfs -put /etc/data/Charges.csv /data/Charges.csv
hdfs dfs -put /etc/data/Inspections.csv /data/Inspections.csv

